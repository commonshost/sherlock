# @commonshost/sherlock 🕵

Dependency tracing tool for HTML, CSS, and JS files. This can be used with HTTP/2 Server Push to serve websites faster.

## Usage

Provide a directory path to the folder that includes HTML, CSS, and JS files.

Sherlock returns a `Promise` which resolves with an array of errors and dependency objects.

Each dependency object has the properties `type`, `parent`, and `path`.

Image dependencies may include the `loading` property, if set in the HTML source.

Each error object has the properties `error` and `parent`.

```js
const sherlock = require('@commonshost/sherlock')

const dependencies = await sherlock(dir)
// [
//   {
//     type: 'application/javascript',
//     parent: '/public/app.js',
//     path:'/public/src/lib.js'
//   },
//   ...
// ]
```

### `error`

An Error instance containing some information about the problem.

Example:

`SyntaxError` from [Acorn](https://github.com/acornjs/acorn#main-parser) with `pos` and `loc` properties and a meaningful `message` string.

### `type`

A string containing the MIME type of the dependency. Can be a specific MIME type of a wildcard.

Examples:

- `application/javascript`
- `image/png`
- `image/*`
- `font/*`
- `text/css`

### `parent`

A string containing the absolute path to the file referencing the dependency.

Examples:

- `/some/where/index.html`
- `/some/where/app.js`
- `/some/where/styles.css`

### `path`

A string containing the absolute path to the referenced file, or external URL. Includes any query string (i.e. `?query`) or hash fragment (i.e. `#hash`).

Examples:

- `/some/where/images/photo.jpeg`
- `/some/where/assets/typeface.woff2`
- `/some/where/assets/typeface.woff2?query-string`
- `/some/where/assets/typeface.woff2#hash-fragment`
- `https://example.net/external-resource`

### `loading`

A string containing the value of the `loading` HTML attribute. Should be either `lazy` or `eager`, but the actual HTML value is provided even if it differs so the [robustness principle](https://en.wikipedia.org/wiki/Robustness_principle) applies.

## See Also

- [HTTP/2 Server Push Manifest](https://gitlab.com/commonshost/manifest)

## Colophon

Made with ❤️ by [Sophie Yang](https://gitlab.com/sophieyang0813) and [Sebastiaan Deckers](https://twitter.com/sebdeckers) for the 🐑 [Commons Host](https://commons.host) project.
