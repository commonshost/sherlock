const { promisify } = require('util')
const mm = require('micromatch')
const fs = require('fs')
const { Parser: HtmlParser } = require('htmlparser2')
const { parse: parseCss, walk: walkCss } = require('css-tree')
const { parse: parseJs } = require('acorn')
const { simple: walkJs } = require('acorn-walk')
const glob = require('glob')
const path = require('path')

const isAbsolutePath = /^\//
const isAbsoluteUrl = /^(https?:)?\/\//i
function resolvePath (base, parent, dependency) {
  if (isAbsoluteUrl.test(dependency)) {
    return dependency
  } else if (isAbsolutePath.test(dependency)) {
    return path.join(base, dependency)
  } else {
    const from = path.dirname(parent)
    return path.resolve(from, dependency)
  }
}

const startQuotes = /^['"]/
const endQuotes = /['"]$/
function cleanupTrimQuotes (value) {
  return value
    .trim()
    .replace(startQuotes, '')
    .replace(endQuotes, '')
}

const isDataUrl = /^data:/

module.exports = async function (dir) {
  const results = []
  const fileResults = await promisify(glob)(dir + '/**/*')

  const filenames = fileResults
  // 1. HTML
  const htmlFiles = mm(filenames, ['**/*.html'])
  const readFile = promisify(fs.readFile)

  for (const html of htmlFiles) {
    const data = await readFile(html, 'utf8')
    const parser = new HtmlParser({
      onopentag: function (name, attribs) {
        if (name === 'link' && attribs.rel === 'stylesheet' && attribs.href) {
          results.push({
            type: 'text/css',
            parent: html,
            path: resolvePath(dir, html, attribs.href)
          })
        } else if (name === 'script' && attribs.src) {
          results.push({
            type: 'application/javascript',
            parent: html,
            path: resolvePath(dir, html, attribs.src)
          })
        } else if (name === 'link' && attribs.rel === 'icon' && attribs.href) {
          results.push({
            type: 'image/*',
            parent: html,
            path: resolvePath(dir, html, attribs.href)
          })
        } else if (name === 'img' && attribs.src) {
          const { loading } = attribs
          results.push({
            type: 'image/*',
            parent: html,
            path: resolvePath(dir, html, attribs.src),
            ...(loading && { loading })
          })
        }
      }
    }, { decodeEntities: true })
    parser.write(data)
    parser.end()
  }
  // 2. CSS
  const cssFiles = mm(filenames, ['**/*.css'])
  for (const css of cssFiles) {
    const cssData = await readFile(css, 'utf8')
    const ast = parseCss(cssData)

    walkCss(ast, function (node) {
      if (node.type === 'Atrule' && node.name === 'import') {
        const url = node.prelude.children.head.data
        const { value } = url.type === 'Url' ? url.value : url
        const cleaned = cleanupTrimQuotes(value)
        const absolute = resolvePath(dir, css, cleaned)
        results.push({
          type: 'text/css',
          parent: css,
          path: absolute
        })
      } else if (node.type === 'Declaration' && node.value.type === 'Value') {
        for (
          let child = node.value.children.head;
          child;
          child = child.next
        ) {
          if (child.data.type === 'Url') {
            const value = child.data.value.value
            const cleaned = cleanupTrimQuotes(value)
            if (isDataUrl.test(cleaned)) {
              continue
            }
            const absolute = resolvePath(dir, css, cleaned)
            const type = node.property === 'src' ? 'font/*' : 'image/*'
            results.push({
              type,
              parent: css,
              path: absolute
            })
          }
        }
      }
    })
  }

  // 3. JS
  const jsFiles = mm(filenames, ['**/*.js'])

  for (const js of jsFiles) {
    const jsData = await readFile(js, 'utf8')
    let astJS
    try {
      astJS = parseJs(jsData, {
        ecmaVersion: 10,
        sourceType: 'module',
        allowReserved: true,
        allowReturnOutsideFunction: true,
        allowImportExportEverywhere: true,
        allowAwaitOutsideFunction: true,
        allowHashBang: true
      })
    } catch (error) {
      results.push({
        error,
        parent: js
      })
      return
    }

    walkJs(astJS, {
      ImportDeclaration (node) {
        if (node.source.type === 'Literal' && typeof node.source.value === 'string') {
          results.push({
            type: 'application/javascript',
            parent: js,
            path: resolvePath(dir, js, node.source.value)
          })
        }
      },
      CallExpression (node) {
        if (
          (
            node.callee.type === 'Identifier' &&
          node.callee.name === 'importScripts'
          ) ||
            (
              node.callee.type === 'MemberExpression' &&
              (
                node.callee.object.type === 'Identifier' &&
                node.callee.object.name === 'self' &&
                node.callee.property.type === 'Identifier' &&
                node.callee.property.name === 'importScripts'
              )
            )
        ) {
          for (const argument of node.arguments) {
            results.push({
              type: 'application/javascript',
              parent: js,
              path: resolvePath(dir, js, argument.value)
            })
          }
        }
      }
    })
  }

  return results
}
